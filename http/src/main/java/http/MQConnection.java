package http;

import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import resources.ReadConfig;


public class MQConnection {
	private static MQConnection singletonInstance = new MQConnection();
	private Connection connection;
	
	private MQConnection() {
		HashMap<String, Object> properties = getProperties();
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost((String) properties.get("host"));
			factory.setPort((Integer) properties.get("port"));
			factory.setUsername((String) properties.get("user"));
			factory.setPassword((String) properties.get("password"));
			connection = factory.newConnection();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private HashMap<String, Object> getProperties() {
		ReadConfig reader = new ReadConfig();
		HashMap<String, Object> properties = new HashMap<String, Object>();
		try {
			properties.put("host", reader.getPropValues("rabbitmqhost"));
			properties.put("port", Integer.parseInt(reader.getPropValues("rabbitmqport")));
			properties.put("user", reader.getPropValues("rabbitmquser"));
			properties.put("password", reader.getPropValues("rabbitmqpassword"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return properties;
	}
	
	public static MQConnection getInstance() {
		return singletonInstance;
	}
	
	public Channel createChannel() {
		try {
			return connection.createChannel();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Connection getConnection() {
		return connection;
	}
	
}
