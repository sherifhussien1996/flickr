package http;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.router.Router;
import io.netty.util.CharsetUtil;

public class JSONHandler extends SimpleChannelInboundHandler<Object> {

	private final Router<String> router;

	JSONHandler(Router<String> router) {
		this.router = router;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
		ByteBuf buffer = (ByteBuf) o;
		JSONObject jsonObject = new JSONObject(buffer.toString(CharsetUtil.UTF_8));
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			String replyQueueName = channel.queueDeclare().getQueue();
			String requestQueueName = "" + (String) jsonObject.get("handler");
			String requestFunctionName = (String) jsonObject.get("function");
			System.out.println(" [x] Requesting " + requestFunctionName + " from " + requestQueueName + "with ID: ");
			String corrId = UUID.randomUUID().toString();
			System.out.println(corrId);
			AMQP.BasicProperties props = new AMQP.BasicProperties.Builder().correlationId(corrId)
					.replyTo(replyQueueName).build();

			channel.basicPublish("", requestQueueName, props, jsonObject.toString().getBytes("UTF-8"));

			final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);

			channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					if (properties.getCorrelationId().equals(corrId)) {
						response.offer(new String(body, "UTF-8"));
					}
				}
			});
			String response_string = response.take();
			System.out.println(" [.] Got '" + response_string + "'");
			ByteBuf content = Unpooled.copiedBuffer(response_string, CharsetUtil.UTF_8);
			FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
					content);
			connection.close();
			channelHandlerContext.write(httpResponse);

		} catch (Exception e) {
			ByteBuf content = Unpooled.copiedBuffer("{\"error\": \"there's something wrong your request\"}",
					CharsetUtil.UTF_8);
			FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
					HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
			response.headers().set("Content-Type", "application/json");
			channelHandlerContext.write(response);
		}
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

}
