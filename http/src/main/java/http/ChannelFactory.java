package http;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class ChannelFactory implements PooledObjectFactory<Channel> {
	private Connection connection;
	
	public ChannelFactory(Connection connection) {
		this.connection = connection;
	}
	@Override
	public PooledObject<Channel> makeObject() throws Exception {
		return new DefaultPooledObject<Channel>(connection.createChannel());
	}

	@Override
	public void destroyObject(PooledObject<Channel> p) throws Exception {
		Channel channel = p.getObject();
		if(channel.isOpen()) {
			channel.close();
		}
		
	}

	@Override
	public boolean validateObject(PooledObject<Channel> p) {
		Channel channel = p.getObject();
		return channel.isOpen();
	}

	@Override
	public void activateObject(PooledObject<Channel> p) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void passivateObject(PooledObject<Channel> p) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
