package gallery_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class DeleteGallery implements Command {
	
	private PostgresService db;
	
	public DeleteGallery() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		res.put("status", "200");
		try {
			JSONObject sqlObj = deleteGallery(request);
			String resMsg = "Sucessfully deleted gallery with ID: " + request.getString("gallery_id");
			if (sqlObj != null) {
				resBody.put("user_msg", resMsg);
				resBody.put("result", sqlObj);
			} else {
				resBody.put("user_msg", "Failed to search search.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to fetch search.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONObject deleteGallery(JSONObject request) throws Exception {
		try {
			String gallery_id = request.getString("gallery_id");
			
			String sql = "select delete_gallery ('%s')";
			String resSQL = String.format(sql, gallery_id);
			System.out.println(resSQL);
			return db.execute(resSQL);
		} catch (Exception e) {
			throw e;
		}
	}
}
