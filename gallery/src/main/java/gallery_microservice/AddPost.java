package gallery_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class AddPost implements Command {

	private PostgresService db;

	public AddPost() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			JSONObject sqlObj = addPost(request);
			res.put("status", "200");
			String resMsg = "Sucessfully added post with ID: " + request.getString("post_id")+ " to gallery with ID: " + request.getString("gallery_id");

			if (sqlObj != null) {
				resBody.put("user_msg", resMsg);
				resBody.put("result", sqlObj);
			} else {
				resBody.put("user_msg", "Failed to add post.");
				resBody.put("error", "Database not updated.");
				res.put("status", "400");
			}
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to add post.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}
	
	public JSONObject addPost(JSONObject request) throws Exception {

		try {
			String post_id = request.getString("post_id");
			String gallery_id;
			try {
				gallery_id = request.getString("gallery_id");
				String sql = "select add_post_gallery('%s', '%s')";
				String resSQL = String.format(sql, post_id, gallery_id);
				System.out.println(resSQL);
				return db.execute(resSQL);
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
