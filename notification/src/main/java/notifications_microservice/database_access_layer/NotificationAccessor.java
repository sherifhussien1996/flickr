package notifications_microservice.database_access_layer;

import java.util.ArrayList;
import java.util.Collection;

import nosql_microservice.ArangoService;
import notifications_microservice.entities.Notification;

public class NotificationAccessor implements NotificationsDAO {
	private ArangoService arango;
	
	public NotificationAccessor(ArangoService arangoService) {
		this.arango = arangoService;
	}

	@Override
	public int createNotification(Notification notification) {
		String key = arango.createDocument("notification", notification.toJson());
		return Integer.parseInt(key);
	}

	@Override
	public void markNotificationAsRead(int userId, int notificationId) {
		return;
	}

	@Override
	public void deleteNotificationById(int userId, int notificationId) {
		arango.deleteDocument("notification", String.valueOf(notificationId));
	}

	@Override
	public Collection<Notification> getUserNotifications(int id) {
		return new ArrayList<Notification>();
	}
}
