package notifications_microservice;

import java.util.Collection;

import org.json.JSONObject;

import command.Command;
import notifications_microservice.database_access_layer.NotificationsDAO;
import notifications_microservice.entities.Notification;
import service_locator.ServiceLocator;

public class GetNotifications implements Command {
	private NotificationsDAO notificationsService;
	
	public GetNotifications() {
		this.notificationsService = ServiceLocator.getInstance().getServiceByName("notifications");
	}
	
	@Override
	public JSONObject run(JSONObject object) {
		JSONObject response = new JSONObject();
		try {
			int userId = object.getInt("user_id");
			Collection<Notification> notifications = notificationsService.getUserNotifications(userId);
			response.put("status", 200);
			response.put("message", new JSONObject().put("result", notifications));
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", 500);
			response.put("message", new JSONObject().put("error",  "internal server error"));
		}
		return response;
	}

}
