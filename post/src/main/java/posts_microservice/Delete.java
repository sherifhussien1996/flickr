package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Delete implements Command {
	private ArangoService arango;
	
	public Delete() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            int postid = object.getInt("id");
            System.out.println(postid);
            Map<String, Object> bindvars = new HashMap<String, Object>();
            bindvars.put("id", postid);
            String query = "FOR t IN firstCollection FILTER t.id == @id "
                    + "REMOVE t IN firstCollection LET removed = OLD RETURN removed";
            JSONObject queryRes = arango.deleteDocument(query,bindvars);

            System.out.println(queryRes);
            res.put("status", "200");
            res.put("message",new JSONObject().put("result",true));

        } catch (Exception e) {
//			throw e;
            res.put("status", "500");
            res.put("message",new JSONObject().put("error","error finding posts"));
        }
        return res;
    }
}
