package posts_microservice;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class Like implements Command {
	private ArangoService arango;
	
	public Like() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            int userID = object.getInt("user_id");
            int postID = getPostId(object.get("uri").toString());
            String userName = object.getString("user_name");
            Map<String, Object> bindVariables = new HashMap<String, Object>();
            bindVariables.put("post_id", "Posts/" + postID);
            bindVariables.put("user_id", userID);
            bindVariables.put("user_name", userName);
            String checkLikeExistQuery = "FOR post IN Posts FILTER post._id == @post_id " +
                    "UPDATE post WITH " +
                    "{likes: PUSH (post.likes, {\"user_id\": @user_id, \"user_name\": @user_name})} " +
                    "IN Posts\n";
            arango.query(checkLikeExistQuery, bindVariables);
            res.put("status", "200");
            res.put("message",new JSONObject().put("result",true));
        } catch (Exception e) {
            res.put("status", "500");
            res.put("message",new JSONObject().put("error", "Failed to like post."));
        }
        return res;
    }

    private int getPostId(String uri) {
        String resource = "posts";
        String[] splittedUri = uri.split("/");
        int idIndex = Arrays.asList(splittedUri).indexOf(resource) + 1;
        return Integer.parseInt(splittedUri[idIndex]);
    }
}
