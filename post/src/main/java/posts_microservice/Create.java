package posts_microservice;

import command.Command;
import org.json.JSONObject;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Create implements Command {
	private ArangoService arango;
	
	public Create() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
    @Override
    public JSONObject run(JSONObject object) {
        JSONObject res = new JSONObject();
        try {
            arango.init();
            JSONObject post = fillJSON(object);
            res.put("status", "200");
            res.put("message",new JSONObject().put("result",arango.createDocument("Posts", post)));
        } catch (Exception e) {
            res.put("status", "500");
            res.put("message", new JSONObject().put("error", "Error creating post."));
        }
        return res;
    }

    private JSONObject fillJSON(JSONObject object) {
        JSONObject json = new JSONObject();
        HashSet<String> new_keys = getNewKeys();
        for (String key : new_keys) {
            if (object.has(key))
                json.put(key, object.get(key));
        }
        HashMap<String,Object> defaults = getDefault();
        for (Map.Entry<String,Object> map : defaults.entrySet())
            json.put(map.getKey(), map.getValue());
        return json;
    }

    private HashSet<String> getNewKeys() {
        HashSet<String> keys = new HashSet<>();
        keys.add("user_id");
        keys.add("name");
        keys.add("type");
        keys.add("hashtags");
        keys.add("link");
        keys.add("tagged");
        return keys;
    }

    private HashMap<String, Object> getDefault() {
        HashMap<String,Object> map = new HashMap<>();
        String currentTime = currentTime();
        map.put("created_at", currentTime);
        map.put("update_at", currentTime);
        map.put("safe", true);
        return map;
    }

    public String currentTime (){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return (sdf.format(cal.getTime()) );
    }
}