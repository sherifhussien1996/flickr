package discussion_tests;

import org.json.JSONObject;
import org.junit.Test;

import configuration.Configurations;
import discussions_microservice.CreateDiscussion;
import discussions_microservice.UpdateDiscussion;
import junit.framework.Assert;

public class DiscussionTest {
	
	@Test
	public void test() throws Exception {
		Configurations.defaultConfig();
		
		//test create discussion
		JSONObject req = new JSONObject();
		JSONObject user = new JSONObject();
		JSONObject discussion = new JSONObject();
		user.put("id", "Dummy User ID");
		discussion.put("title", "Hello World");
		discussion.put("post_text", "This is post text");
		discussion.put("group_id", "dummy group id");
		discussion.put("status", true);
		req.put("user", user);
		req.put("discussion", discussion);
		CreateDiscussion cd = new CreateDiscussion();
		JSONObject res = cd.run(req);
		Assert.assertEquals(res.getString("status"), "200");
		String discussionID = res.getJSONObject("message").getString("result");
		
		//test update discussion
		discussion.put("post_text", "This is post text after update");
		discussion.put("_key", discussionID);
		req.put("user", user);
		req.remove("discussion");
		req.put("discussion", discussion);
		UpdateDiscussion ud = new UpdateDiscussion();
		res = ud.run(req);
		Assert.assertEquals(res.getString("status"), "200");
		
	}

}
