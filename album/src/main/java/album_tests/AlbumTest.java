package album_tests;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import configuration.Configurations;
import album_microservice.*;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;


public class AlbumTest {
	
	
	@Test
	public void test() throws Exception {
		Configurations.defaultConfig();
		PostgresService db = ServiceLocator.getInstance().getServiceByName("pg");

		JSONObject req = new JSONObject();
		JSONObject user = new JSONObject();
		
		//create album
		req.put("user_id", "1");
		req.put("name", "dummy album");
		req.put("description","dummy desc");
		CreateAlbum cg = new CreateAlbum();
		JSONObject res= cg.run(req);
		Assert.assertEquals("200", res.getString("status"));
	
//		
////		//test add post
		req = new JSONObject();
		req.put("post_id","111");
		req.put("album_id","1");
		AddPost ap = new AddPost();
		res= ap.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		req = new JSONObject();
		
//		//test delete post
//		
		req = new JSONObject();
		req.put("post_id","111");
		req.put("album_id","1");
		DeletePost dp = new DeletePost();
		
		res= dp.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		//test favorite album 
		req = new JSONObject();
		req.put("user_id","1");
		req.put("album_id","1");
		FavoriteAlbum fg = new FavoriteAlbum();
		
		res= fg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//view  album
		
		req = new JSONObject();
		req.put("user_id","1");
		SearchFavorite vf = new SearchFavorite();
		
		res= vf.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		
		

//		
//		
//		//test unfavorite album 
//		
		req = new JSONObject();
		req.put("user_id","1");
		req.put("album_id","1");
		UnFavoriteAlbum ufg = new UnFavoriteAlbum();
		
		res= ufg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
//		
//		
//		//view albums
		req = new JSONObject();
		req.put("user_id","1");
		ViewAlbum vg = new ViewAlbum();
		
		res= vg.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
		//view Posts
		req = new JSONObject();
		req.put("album_id","1");
		ViewPosts vp = new ViewPosts();
		
		res= vp.run(req);
		Assert.assertEquals("200", res.getString("status"));
		
//		
	}
}
