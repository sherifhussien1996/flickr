package album_microservice;

import org.json.JSONArray;
import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class  SearchFavorite implements Command {
	
	PostgresService db;
	
	public SearchFavorite() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		
		try {
			JSONArray temp = viewFavoriteAlbum(request);
			if (temp!=null) {
				res.put("status", "200");
				String resMsg ="Sucessfully fetched favorite Albums";
				resBody.put("user_msg", resMsg);
				resBody.put("result", temp);
				
			} else {
				res.put("status", "400");
				resBody.put("user_msg", "Failed to fetch favorite album.");
				resBody.put("error", "No rows updated");
			}
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to fetch favorite album.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;	
	}
	
	public JSONArray viewFavoriteAlbum(JSONObject album) throws Exception{
		JSONArray q = new JSONArray();
		try {
			String user_id = album.getString("user_id");
			String sql = "select view_favorite_albums ('%s')";
			String resSQL = String.format(sql, user_id);
			System.out.println(db.execute(resSQL));
			q = db.execute(resSQL).getJSONArray("Result");
			System.out.println(q);
		} catch (Exception e) {
			throw e;
		}

		return q;
	}

}
