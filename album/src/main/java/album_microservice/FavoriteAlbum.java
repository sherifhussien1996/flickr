package album_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class FavoriteAlbum implements Command {
	
	private PostgresService db;
	
	public FavoriteAlbum() {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		db = serviceLocator.getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		
		try {
			boolean success = favoriteAlbum(request);
			
			if (success) {
				
				res.put("status", "200");
				String resMsg ="Sucessfully added album with ID: " + request.getString("album_id")+ " to favorites of the user with ID: " + request.getString("user_id");
				resBody.put("user_msg", resMsg);
				
			} else {
				res.put("status", "400");
				resBody.put("user_msg", "Failed to add favorite album.");
				resBody.put("error", "No rows updated");

			}
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to add favorite album.");
			resBody.put("error", e.getMessage());
		}
		res.put("message", resBody);
		return res;	
	}
	
	public Boolean favoriteAlbum(JSONObject request) throws Exception{

		try {
			String user_id = request.getString("user_id");
			String album_id = request.getString("album_id");

			String sql = "select favorite_album('%s', '%s')";
			String resSQL = String.format(sql, user_id, album_id);
			JSONObject resObj = db.execute(resSQL);

			if (resObj == null) {
				return false;
			}
			System.out.println(resObj);
		} catch (Exception e) {
			throw e;
		}

		return true;
	}

}
