package album_microservice;

import org.json.JSONObject;
import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class DeleteAlbum implements Command {
	
	private PostgresService db;
	
	public DeleteAlbum() {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		db = serviceLocator.getServiceByName("pg");
	}

	public JSONObject run(JSONObject album) {
		JSONObject res = new JSONObject();
		JSONObject resBody = new JSONObject();
		
		try {
			boolean success = deleteAlbum(album);

			if (success) {
				res.put("status", "200");
				String resMsg ="Sucessfully deleted album with ID: " + album.getString("album_id");
				resBody.put("user_msg", resMsg);
			} else {
				res.put("status", "400");
				resBody.put("user_msg", "Failed to delete album.");
				resBody.put("error", "No rows updated");

			}
		} catch (Exception e) {
			res.put("status", "400");
			resBody.put("user_msg", "Failed to delete album.");
			resBody.put("error", e.getMessage());
		}
		
		res.put("message", resBody);
		return res;	
	}
	
	public Boolean deleteAlbum(JSONObject album) throws Exception{

		try {
			String album_id = album.getString("album_id");
			
			String sql = "select delete_album ('%s')";
			String resSQL = String.format(sql, album_id);
			System.out.println(resSQL);
			JSONObject resObj = db.execute(resSQL);

			if (resObj == null) {
				return false;
			}
			System.out.println(resObj);
		} catch (Exception e) {
			throw e;
		}

		return true;
	}

}
