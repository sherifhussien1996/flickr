package service_locator;

import java.util.concurrent.ConcurrentHashMap;

import exceptions.ServiceStorageException;

public class ServiceLocator {
	private static ServiceLocator instance;
	
	private ConcurrentHashMap<String, Object> registry;
	
	
	private ServiceLocator() {
		this.registry = new ConcurrentHashMap<String, Object>();
	}
	
	public static ServiceLocator getInstance() {
		if(instance == null)
			instance = new ServiceLocator();
		return instance;
	}
	
	public void storeService(String serviceName, Object service) throws ServiceStorageException {
		if(this.registry.containsKey(serviceName))
			throw new ServiceStorageException("This service already exists");
		this.registry.put(serviceName, service);
	}
	
	public void updateService(String serviceName, Object service) {
		this.registry.put(serviceName, service);
	}
	
	public <T> T getServiceByName(String name) {
		return (T) this.registry.get(name);
	}
}
