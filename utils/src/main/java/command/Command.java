package command;

import org.json.JSONObject;


public interface Command {
	public JSONObject run(JSONObject object);
}
