package command;

import org.json.JSONObject;
import rx.annotations.Experimental;

@Experimental
public abstract class ChainableCommand implements Command {
	private boolean failFast;
	private ChainableCommand next;
	
	public ChainableCommand(boolean failFast) {
		this.failFast = failFast;
	}
	
	public ChainableCommand() {
		this(true);
	}
	
	public void chain(ChainableCommand command) {
		if(this.next == null)
			this.next = command;
		else
			next.chain(command);
	}
	
	public void run(CommandContextObject context) {
		if(this.failFast && context.errorCount() > 0)
			if(next != null)
				next.run(context);
		this.execute(context);			
	}
	
	public abstract JSONObject execute(CommandContextObject context);
}
