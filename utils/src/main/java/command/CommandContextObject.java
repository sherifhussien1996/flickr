package command;

import java.util.Collection;
import java.util.HashMap;

import rx.annotations.Experimental;

@Experimental
public class CommandContextObject {
	private HashMap<String, Object> state;
	private Collection<Exception> exceptions;
	
	public <T> T getItem(String key) {
		return (T) state.get(key);
	}
	
	public void storeItem(String key, Object item) {
		this.state.put(key, item);
	}
	
	public boolean errored() {
		return exceptions.size() > 0;
	}
	
	public void error(Exception e) {
		this.exceptions.add(e);
	}
	
	public int errorCount() {
		return this.exceptions.size();
	}
}
