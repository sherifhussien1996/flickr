package configuration;

import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cache.CacheService;
import org.json.JSONException;
import org.json.JSONObject;

import command_provider.CommandProvider;
import exceptions.ServiceStorageException;
import nosql_microservice.ArangoService;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class Configurations {
	public static void defaultConfig() throws ServiceStorageException, ClassNotFoundException, JSONException, IOException {
		/** TODO:
		 * 1-move init logic to a facade
		 * 2-load from config --DONE
		 * 3-consider a testing env(init service locator with mock objects) --DONE
		 * 4-Async db queries if there is time
		**/
		ServiceLocator globalLocator = ServiceLocator.getInstance();

		AppConfig appConfig;
		String environmentType = System.getenv("ENV_TYPE");
		if(environmentType != null && environmentType.equals("Deployment"))
			appConfig = AppConfig.loadFromEnv();
		else
			appConfig = AppConfig.loadFromPropsFile("/config.properties");
		
	
		ExecutorService globalExecutor = Executors.newFixedThreadPool(
				Integer.parseInt(appConfig.getConfigVar("NUM_THREADS")));
		
		globalLocator.storeService("executor", globalExecutor);
		
		globalLocator.storeService("config", appConfig);
		
		PostgresService pgService = new PostgresService(
				appConfig.getConfigVar("db_url"),
				appConfig.getConfigVar("db_user"),
				appConfig.getConfigVar("db_password"),
				appConfig.getConfigVar("db_driver"),
				Integer.parseInt(appConfig.getConfigVar("db_initial_pool_size")),
				Integer.parseInt(appConfig.getConfigVar("db_max_pool_size"))
		);
		pgService.connect();
		ArangoService arangoService = new ArangoService(
				appConfig.getConfigVar("nosqldb_host"),
				Integer.parseInt(appConfig.getConfigVar("nosqldb_port")),
				appConfig.getConfigVar("nosqldb_name")
		);
		arangoService.init();

		HashSet<HostAndPort> set = new HashSet<>();
//		System.out.println(appConfig.getConfigVar("cache_host"));
		set.add(new HostAndPort(
				appConfig.getConfigVar("cache_host"),
				Integer.parseInt(appConfig.getConfigVar("cache_port")
				))) ;
		CacheService cacheService = new CacheService(set);
		cacheService.init();

		globalLocator.storeService("cache", cacheService);
		globalLocator.storeService("pg", pgService);
		globalLocator.storeService("arango", arangoService);
		
		CommandProvider commandProvider = CommandProvider.loadFromJSON(new JSONObject(appConfig.getConfigVar("COMMANDS")));
		globalLocator.storeService("commandRegistry", commandProvider);
	}
}
