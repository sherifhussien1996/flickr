package dispatcher;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;

import org.json.JSONException;
import org.json.JSONObject;
import command.Command;
import command_provider.CommandProvider;
import rx.annotations.Experimental;

@Experimental
public class Dispatcher {
	private ExecutorService executor;
	private CommandProvider commandProvider;
	
	public Dispatcher(ExecutorService executor, CommandProvider commandProvider) {
		this.executor = executor;
	}
	
	//TODO: use Logger
	public void dispatchRequest(final byte[] request) {
		executor.execute(new Runnable() {
			public void run() {
				try {
					JSONObject jsonBody = new JSONObject(new String(request, "UTF-8"));
					String commandResourceName = jsonBody.getString("uri");
					//Command command = commandProvider.getCommandInstance(commandResourceName);
					//JSONObject response = command.run(jsonBody);
				} catch(Exception e) {
					System.err.println(e);
				}
			}
		});
	}
}