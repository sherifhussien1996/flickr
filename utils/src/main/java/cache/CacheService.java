package cache;

import configuration.Configurations;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import service_locator.ServiceLocator;

import java.rmi.server.ExportException;
import java.util.HashSet;

public class CacheService {
    private JedisCluster cluster ;
    private HashSet<HostAndPort> list ;
    public CacheService(HashSet<HostAndPort> list) {
        this.list = list ;
    }
    public void init() {
        cluster = new JedisCluster(list) ;
    }

    public String get(String key){
        if(cluster.exists(key))
            return cluster.get(key);
        return "False";
    }
    public String delete(String key){
        if(cluster.exists(key)) {
            cluster.del(key);
            return "True";
        }
        return "False";
    }
    public boolean exists(String key){
        if(cluster.exists(key))
            return true;
        return false ;
    }
    public void set(String key , String value){
        cluster.set(key , value);
    }
}
