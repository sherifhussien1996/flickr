package exceptions;

public class DAOException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DAOException() {
		super();
	}
	
	public DAOException(String msg) {
		super(msg);
	}
	
	public DAOException(Exception wrapped) {
		super(wrapped);
	}
}
