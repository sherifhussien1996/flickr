# Ultimate team - Flickr

This a quick start guide to get up & running into the development of project. 

## Getting Started

### Prequisites

I'll  assume you're working __outside__ of the enviroment given by Osh (your regular OS), 
since most of the team don't have it working. If the VM is working, you __can__ skip this
section.

1. RabbitMQ (<https://www.rabbitmq.com/download.html>)
   Don't forget to start the server after installation. 
   ```
   rabbitmq-server
   ```
   __Note: Run the following command if you're on a MAC and you get an 'error: command not found' 
   message after installation__
   ```
   export PATH=/usr/local/Cellar/rabbitmq/3.7.12/sbin:$PATH
   ```
2. Redis (<https://redis.io/download>)
   Don't forget to start the server after installation. 
   The steps to run the redis cluster can be found inside the redis directory inside redis-5.0.3/utils/create-cluster/README
- For building the redis cluster using docker:
```
   docker-compose build
   docker-compose up or docker-compose up -d
```
__Note: All other dependencies have been preincluded in the project for convenience &
to avoid versioning conflict __

### Importing in Ecplise

* Open eclipse
* Click File > Import
* Type Maven in the search box under Select an import source:
* Select Existing Maven Projects
* Click Next
* Click Browse and select the folder that is the root of the Maven project (folder containing pom.xml file)
* Click Next
* Click Finish

__Note: This process is sometimes extremely slow__
   
## Code structure

The whole application is structured in 5 main components:

1. Load balancers
2. HTTP Servers (ie. front-end)
3. Messaging Queues
4. Microservices (ie. applications/apps)
5. Database

![alt text](architecture.png)
 
 What is currently implemented in this example is a sample microservice (Users) 
 with a sample function (Signup) and an HTTP server to demonstrate the lifecycle of a request.
 Nothing regarding database management or load balancing has been implemented yet.
 Please have a look at the code and try to understand what goes on i'll explain more in person
 
## Test run:

1. Run example_microservice/Users.java (the app/microservice)
2. Run http/NettyHTTPServer.java (the http server)
3. Open Postman and test with the following body (more explanation later):
   ```
   {
	"name": "Bassem is tired!",
	"handler": "example_microservice",
	"function": "TestFunction"
    }
   ```
   
  The request should return whatever is written in the 'name' field.
  
  Goodluck ⭐⭐



 

