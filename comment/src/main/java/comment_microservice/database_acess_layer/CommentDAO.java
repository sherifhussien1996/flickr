package comment_microservice.database_acess_layer;

import comment_microservice.entities.Comment;
import exceptions.DAOException;

public interface CommentDAO {
	int createComment(Comment comment);
	Comment getCommentById(int id) throws DAOException;
	void addLikeForComment(int userId, int commentId) throws DAOException;
	void removeLikeFromComment(int userId, int commentId) throws DAOException;
}
