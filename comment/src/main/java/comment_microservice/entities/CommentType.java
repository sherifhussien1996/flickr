package comment_microservice.entities;

import comment_microservice.exceptions.UnknowCommentTypeException;

public enum CommentType {
	GALLERY_COMMENT,
	DISCUSSION_COMMENT,
	POST_COMMENT;
	
	public static CommentType fromString(String str) throws UnknowCommentTypeException {
		if(str.equalsIgnoreCase("gallery") || str.equals("GALLERY_COMMENT"))
			return GALLERY_COMMENT;
		if(str.equalsIgnoreCase("discussion") || str.equals("DISCUSSION_COMMENT"))
			return DISCUSSION_COMMENT;
		if(str.equalsIgnoreCase("post") || str.equals("POST_COMMENT"))
			return POST_COMMENT;
		throw new UnknowCommentTypeException("Unrecognized comment type: " + str);
	}
}
