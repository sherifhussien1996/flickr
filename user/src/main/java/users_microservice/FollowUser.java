package users_microservice;

import org.json.JSONObject;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class FollowUser implements Command {
	private PostgresService db;

	public FollowUser() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		JSONObject res =  new JSONObject();
		JSONObject resBody = new JSONObject();
		try {
			res.put("status", "200");
			String resMsg = "Sucessfully followed User with ID: " + request.getString("followed_id");
			JSONObject sqlObj = followUser(request);
			// TODO: check if any updated rows
			resBody.put("user_msg", resMsg);
			resBody.put("result", sqlObj);
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to follow user");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}

	public JSONObject followUser(JSONObject request) throws Exception {

		try {
			String myID = request.getString("user_id");
			String otherUserID = request.getString("followed_id");

			String sql = "select follow_user ('%s', '%s')";
			String resSQL = String.format(sql, myID, otherUserID);
			System.out.println(resSQL);
			return db.execute(resSQL);

		} catch (Exception e) {
			throw e;
		}

	}

}
