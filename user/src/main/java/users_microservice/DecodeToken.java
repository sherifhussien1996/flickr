package users_microservice;

import org.json.JSONObject;

import command.Command;

public class DecodeToken implements Command {

	@Override
	public JSONObject run(JSONObject object) {
		return (new JWTService()).decode(object.getString("token"));
	}

}
