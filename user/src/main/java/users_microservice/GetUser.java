package users_microservice;

import org.json.JSONArray;
import org.json.JSONObject;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class GetUser implements Command {

	private PostgresService db;

	public GetUser() {
		this.db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	@Override
	public JSONObject run(JSONObject object) {
		JSONObject res = new JSONObject();
		res = new JSONObject();
		JSONObject resBody = new JSONObject();

		try {
			res.put("status", "200");
			int userid = object.getInt("id");
			System.out.println(userid);
			JSONObject queryRes = db.execute(String.format("select * from users where id = '%s'", userid));
			JSONArray queryArray = (JSONArray) queryRes.get("Result");
			System.out.println(queryRes);
			JSONObject singleUser = (JSONObject) queryArray.get(0);
			singleUser.remove("password");
			resBody.put("user", singleUser);
		} catch (Exception e) {
			resBody.put("user_msg", "Failed to find user.");
			resBody.put("error", e.getMessage());
			res.put("status", "400");
		}
		res.put("message", resBody);
		return res;
	}

}
