package groups_microservice;

import org.json.JSONObject;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

public class DeleteGroup implements Command {

	private ArangoService arango;
	
	public DeleteGroup() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
	public JSONObject run(JSONObject request) {
		//Authentication Still needed
		JSONObject user;
		String groupID;
		JSONObject res = new JSONObject();
		try {
			user = (JSONObject) request.get("user");
			groupID = request.getString("_id");
		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "400");
			return res;
		}
		try{
			JSONObject group = (JSONObject)arango.readDocument("Groups", groupID).get("result");
			System.out.println(group);
			if(!group.getString("admin_id").equals(user.getString("_id"))) {
				res.put("message", new JSONObject().put("error", "Unauthorized to delete this group"));
				res.put("status", "401");
				return res;
			}
			arango.deleteDocument("Groups", groupID);
			res.put("message", new JSONObject().put("result", "Group deleted"));
			res.put("status", "200");
			return res;
		} catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
			return res;	
		}
	}
}
