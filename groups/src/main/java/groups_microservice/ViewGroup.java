package groups_microservice;

import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

import org.json.JSONObject;

import command.Command;

public class ViewGroup implements Command {

	private ArangoService arango;
	
	public ViewGroup() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
	public JSONObject run(JSONObject request) 
	{
		//Authentication Still needed		
		String groupID;
		JSONObject group = new JSONObject();
		JSONObject res = new JSONObject();
		try {
			groupID = request.getString("_id");
		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "400");
			return res;
		}
		try {
			arango.init();
			group = arango.readDocument("Groups", groupID);
			res.put("message", group);
			res.put("status", "200");
			return res;
		} catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
				return res;
		}
	}
}