package groups_microservice;

import org.json.JSONObject;

import com.arangodb.entity.BaseDocument;

import command.Command;
import nosql_microservice.ArangoService;
import service_locator.ServiceLocator;

public class CreateGroup implements Command {

	private ArangoService arango;
	
	public CreateGroup() {
		this.arango = ServiceLocator.getInstance().getServiceByName("arango");
	}
	
	public JSONObject run(JSONObject request) {
		//Authentication Still needed
		JSONObject user;
		JSONObject res = new JSONObject();
		JSONObject group = new JSONObject();
		BaseDocument groupObj = new BaseDocument();
		try {
			user = (JSONObject) request.get("user");
			group.put("admin_id", user.getString("_id"));
			group.put("title", request.getString("title"));
			groupObj.addAttribute("admin_id", user.getString("_id"));
			groupObj.addAttribute("title", request.getString("title"));

		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "400");
			return res;
		}
		try {
			String groupID = arango.createDocument("Groups", groupObj);
			group.put("_id", groupID);
			res.put("message", new JSONObject().put("result", group));
			res.put("status", "200");
			return res;
		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
			return res;		
		}
	}
}
