package groups_microservice;

import org.json.JSONObject;

import command.Command;
import service_locator.ServiceLocator;
import sql_microservice.PostgresService;

public class JoinGroup implements Command {
	
	private PostgresService db;
	
	public JoinGroup() {
		db = ServiceLocator.getInstance().getServiceByName("pg");
	}

	public JSONObject run(JSONObject request) {
		//Authentication Still needed
		JSONObject user = (JSONObject) request.get("user");
		JSONObject res = new JSONObject();
		String userID;
		String groupID;
		try {
			userID = user.getString("_id");
			groupID = request.getString("group_id");
		}catch(Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "400");
			return res;
		}
		String sql = "select create_user_group('%s', '%s')";
		String resSQL = String.format(sql,  userID, groupID);
		try {
			JSONObject resObj = db.execute(resSQL);
			res.put("message", new JSONObject().put("result", resObj));
			res.put("status", "200");
			return res;
		} catch (Exception e) {
			res.put("message", new JSONObject().put("error", e.getMessage()));
			res.put("status", "500");
			return res;
		}
	}
}
